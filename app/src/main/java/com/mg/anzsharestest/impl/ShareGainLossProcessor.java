package com.mg.anzsharestest.impl;

import android.support.annotation.NonNull;
import android.util.Pair;

import com.mg.anzsharestest.R;
import com.mg.anzsharestest.model.IllegalPricesException;

import java.util.List;

public class ShareGainLossProcessor {

    private List<Double> mClosingPrices;

    public ShareGainLossProcessor(@NonNull List<Double> closingPrices) throws IllegalPricesException {
        mClosingPrices = closingPrices;
        validate();
    }

    /**
     * Calculates the sum-total of gains and sum-total of losses based on daily closing share price values.
     * @return a {@code Pair} of {@code Double} values, where the first member of the pair represents the gains
     *          and the second member - the losses
     */
    public Pair<Double, Double> calculateGainsAndLosses() {
        Pair<Double, Double> result = new Pair<>(0.0, 0.0);

        if (mClosingPrices.size() > 1) {
            double gains = 0;
            double losses = 0;

            for (int i = 1; i < mClosingPrices.size(); i++) {

                double difference = mClosingPrices.get(i) - mClosingPrices.get(i - 1);

                if (difference > 0) {
                    // closing prices have gone up
                    gains += difference;
                } else {
                    //closing prices have gone down
                    losses += difference;
                }
            }

            return new Pair<Double, Double>(gains, losses);
        } else {
            // there is only one closing share price entered. No trend can be seen, therefore assumed that
            // the gain and loss are both 0.
            // Nothing to do in this case, simply return the Pair

            return new Pair<Double, Double>(0.0, 0.0);
        }
    }


    //Section - Validation

    private void validate() throws IllegalPricesException {
        if (mClosingPrices == null || mClosingPrices.size() == 0) {
            throw new IllegalPricesException(
                    "The parameter 'closingPrices' to constructor ShareGainLossProcessor(List<Double>) " +
                                                        "must be non-null, non-empty list of numeric values",
                    R.string.enter_closing_share_prices);
        }
        for (Double value : mClosingPrices) {
            if (value <= 0) {
                throw new IllegalPricesException(
                        "Closing share prices can only be positive values.",
                        R.string.enter_positive_share_prices);
            }
        }
    }
}
