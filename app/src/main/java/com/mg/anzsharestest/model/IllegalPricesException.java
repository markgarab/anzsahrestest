package com.mg.anzsharestest.model;

public class IllegalPricesException
        extends IllegalArgumentException {

    public final int mUserMessageId;

    public IllegalPricesException(int userMessageId) {
        super();
        mUserMessageId = userMessageId;
    }

    public IllegalPricesException(String s, int userMessageId) {
        super(s);
        mUserMessageId = userMessageId;
    }

    public IllegalPricesException(String message, Throwable cause, int userMessageId) {
        super(message, cause);
        mUserMessageId = userMessageId;
    }

    public IllegalPricesException(Throwable cause, int userMessageId) {
        super(cause);
        mUserMessageId = userMessageId;
    }
}
