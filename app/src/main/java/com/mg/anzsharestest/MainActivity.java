package com.mg.anzsharestest;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Pair;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.mg.anzsharestest.impl.ShareGainLossProcessor;
import com.mg.anzsharestest.model.IllegalPricesException;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    /**
     * Ideally, I'd use butterknife library to bind UI components to its properties,
     * however to keep external dependencies to bare minimum, I'll use conventional
     * findViewById() method
     */

    private EditText etClosingShares;

    private Button btnCalculateGainsAndLosses;

    private TextView txtErrorMessages;

    private TextView txtResultsMessage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        etClosingShares = (EditText)findViewById(R.id.text_share_prices);
        btnCalculateGainsAndLosses = (Button)findViewById(R.id.btn_calc_gains_and_losses);
        txtErrorMessages = (TextView)findViewById(R.id.text_error_messages);
        txtResultsMessage = (TextView)findViewById(R.id.text_results);

        btnCalculateGainsAndLosses.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                processCalculateGainsAndLosses();
            }
        });

        etClosingShares.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length() == 0) {
                    btnCalculateGainsAndLosses.setEnabled(false);
                } else {
                    btnCalculateGainsAndLosses.setEnabled(true);
                }
                txtErrorMessages.setVisibility(View.GONE);
                txtResultsMessage.setVisibility(View.GONE);
            }

            @Override
            public void afterTextChanged(Editable s) {}
        });
    }

    private void processCalculateGainsAndLosses() {
        String userInput = etClosingShares.getText().toString().trim();

        if (userInput.isEmpty()) {
            displayError(R.string.enter_closing_share_prices);
        } else {
            String[] prices = userInput.split("\n");

            List<Double> closingSharePrices = new ArrayList<>();
            try {
                for (String price : prices) {
                    closingSharePrices.add(Double.valueOf(price));
                }

                displayResult(new ShareGainLossProcessor(closingSharePrices).calculateGainsAndLosses());
            } catch (NumberFormatException exp) {
                displayError(R.string.enter_positive_share_prices);
            } catch (IllegalPricesException exp) {
                displayError(exp.mUserMessageId);
            }
        }
    }

    private void displayError(int messageId) {
        txtErrorMessages.setText(messageId);
        txtErrorMessages.setVisibility(View.VISIBLE);
    }

    private void displayResult(Pair<Double, Double> gainsAndLosses) {
        txtResultsMessage.setText(getApplicationContext().getString(
                R.string.gains_and_losses_result,
                gainsAndLosses.first,
                gainsAndLosses.second
        ));
        txtResultsMessage.setVisibility(View.VISIBLE);
    }
}
