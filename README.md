Open this document in raw mode

Task:
Given the days' closing share prices calculate the sum-total of the share price gains and the sum-total of share price losses.




Approach:
Maintain two values: one for gains and one for losses.
IF
the next day's closing share price was greater than the previous day's share price then there was a price gain, and the
difference, which is a positive number shall be added to the gains.
OTHERWISE
if the next day's closing share price was lesser than the previous day's share price then there was a price loss, and the
difference, which is a negative number shall be added to the losses.
DISPLAY
the resulting values of gains and losses as the output of the task.




Task Assumptions:
1.
Since we're dealing with price values then each day's closing value can be a positive decimal value ONLY!
(negative and zero values do not make sense in this context, and will be treated as errors in the input)

2.
Assuming that each day's closing value will be input in its own separate line. One line, one value.
 - lines containing non numeric characters are invalid, and will be treated as errors in the input.

 E.G.
 Valid values:
 1
 2.5
 3.65
 2.638

 Invalid values:
     (Empty string, multiple white space strings in between valid values)
 -4  (negative integer value)
 -4.5 (negative decimal value)
 0  (zero value)
 2.5 3.5 (multiple numeric values separated by blank space)
 asda   (non numeric string)
 23w    (string containing non numeric character)
 10e+2  (exponential representation of a decimal value)

 Note:
 Leading and trailing white space lines will be trimmed away automatically.
 If the resulting list of values are valid the code will work as expected.



Programming the task:
the developed code is a fully functional android mobile app that can be readily run on a device or emulator.
- the latest versions of AndroidStudio and Gradle plugin for MacOS are used
- target android SDK version is 28 (P)
- minimum android SDK version is 21 (Lollipop)
Please make sure you have the relevant SDKs plugins and libraries installed on your local machine where you intend to
compile and run the code.
No third party android library is used.
When installed on a device or an emulator please look for a generic launcher icon with "ANZSharesTest" label.